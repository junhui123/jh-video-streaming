**주요 내용**

- 내 컴퓨터의 MP4/MKV 영상을 웹을 통해서 스트리밍


**주요 기능**

메인화면

![수정됨_메인화면.png](https://meeta.io:3000/api/image/1523351427660.png)

- VueJS2를 사용한 SPA 화면 구성

- Axios를 통한 Spring 서버와 연동

- Java8 Stream과 CompletableFuture를 통한 비동기 처리

- 네이버 영화 API를 사용한 영화 정보 처리

- HSQLDB를 통한 간단한 데이터베이스 처리

로그인/로그아웃

![수정됨_login 화면.png](https://meeta.io:3000/api/image/1523351661509.png)

- BCrypt를 통한 비밀번호 암호화

- vue-session을 통한 세션 처리

영화 타이틀 수정

<변경 전>

![수정됨_업데이트 안된 영화 화면.png](https://meeta.io:3000/api/image/1523351661507.png)

<변경 후>

![수정됨_영화 타이틀 업데이트.png](https://meeta.io:3000/api/image/1523351661583.png)

- 타이틀 기준 영화 정보 업데이트 

스트리밍

![수정됨_스트리밍.png](https://meeta.io:3000/api/image/1523355744252.png)

- IIS Media Service 또는 Tomcat을 통한 스트리밍

타이틀 추출

- Python Parse Torrent Name 패키지를 Jython을 통해서 사용