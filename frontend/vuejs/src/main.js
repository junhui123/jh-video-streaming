import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueSession from 'vue-session'
import checkbox from 'vue-material-checkbox'

Vue.use(BootstrapVue);
Vue.use(checkbox)

/* data stored in the session may persist
 * between tabs and browser instances
 * default false  */
var options = {
    persist: true
}
Vue.use(VueSession, options)

Vue.config.productionTip = false

Vue.directive('autofocus', {
  // 바인딩 된 엘리먼트가 DOM에 삽입되었을 때...
  inserted: function (el) {
    // 엘리먼트에 포커스를 줍니다
    el.focus()
  }
})

/* eslint-disable eqeqeq */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
})
