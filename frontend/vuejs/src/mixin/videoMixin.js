import Vue from 'vue'
import axios from 'axios'
import {videos} from '../videos.js'

export const videoMixin = {
	methods: {
		clickFavoriteCheckbox(checkbox, video){
			this.addVideoToFavorite(checkbox, video, checkbox.target.value, this.$session.get('loginedUser'))
		},
		addVideoToFavorite(checkbox, video, vid, uid){
			//즐겨찾기 추가
			if(checkbox.target.checked) {
				axios.post('http://localhost:8080/favorite', {
					videoid_fk: vid,
					uid_fk: uid
				  })
				  .then(function (response) {
					video.FAVORITE=true
				  })
				  .catch(function (error) {
					 alert(error.response.data)
				  })
			}
			//즐겨찾기 해제
			else {
				axios.delete('http://localhost:8080/favorite', { data: { videoid_fk: vid, uid_fk: uid } } )
				.then(function(response){
					delete video.FAVORITE
				})
				.catch(function(error) {
					alert(error.response.data)
				})
			}			
		},
		clearVideosJS(){
			videos.all=[]
			videos.tv=[]
			videos.movie=[]
		},
	}
}