import Vue from 'vue'
import Router from 'vue-router'
import all from '../components/all'
import movie from '../components/movie'
import tv from '../components/tv'
import search from '../components/search'
import login from '../components/login'
import userProfile from '../components/userProfile'

Vue.use(Router)

const routes = [ 
	{
		path: '/',
		redirect: '/login'
	},    
	{
		path: '/login',
	    name: 'login',
	    component: login,
	},    
	{
		path: '/all',
	    name: 'all',
	    component: all,
	},    
	{
	  path: '/movie',
	  name: 'movie',
	  component: movie
	},
	{
	  path: '/tv',
	  name: 'tv',
	  component: tv
	},
	{
	  path: '/search',
	  name: 'search',
	  component: search
	},
	{
		path: '/userProfile',
		name: 'userProfile',
		component: userProfile
	}
]

export default new Router({
  mode: 'history',
  base: '/',
  routes
})

