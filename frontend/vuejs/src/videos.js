export const videos = {
	all: [],
	tv: [],
	movie: [],
	getByType(type) {
		if(type=="all"){
			return this.all
		} else if(type== "tv") {
			return this.tv
		} else if(type=="movie") {
			return this.movie
		}
	},
}