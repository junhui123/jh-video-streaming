package streaming.util;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class PrintUtil {
	public static void printRequestAllParams(HttpServletRequest request) {
		Enumeration params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String key = (String) params.nextElement();
			System.out.println(key + " : " + request.getParameter(key));
		}
	}

	public static void printRequestAllAttribute(HttpServletRequest request) {
		Enumeration params = request.getAttributeNames();
		while (params.hasMoreElements()) {
			String key = (String) params.nextElement();
			System.out.println(key + " : " + request.getAttribute(key));
		}
	}

	public static void printMapKeyValue(Map map) {
		Iterator<String> mapIter = map.keySet().iterator();
		while (mapIter.hasNext()) {
			String key = mapIter.next();
			Object value = map.get(key);
			System.out.println(key + " : " + value);
		}
	}
	
	public static void printList(List list) {
		for(int i=0; i<list.size(); i++) {
			System.out.println(i+"="+list.get(i));
		}
	}
}
