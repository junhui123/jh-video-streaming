package streaming.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import streaming.business.domain.Movie;
import streaming.business.domain.TV;
import streaming.business.domain.Video;
import streaming.business.service.NaverMovieService;

@Component
public class VideoObjectConvertUtil {
	
	private static ParseVideoName parseVideoName;
	
	@Autowired	
	private void setParseVideoName(ParseVideoName parseVideoName) {
		VideoObjectConvertUtil.parseVideoName = parseVideoName;
	}
	
	private static NaverMovieService movieService;
	
	@Autowired	
	private void setNaverMovieService(NaverMovieService movieService) {
		VideoObjectConvertUtil.movieService = movieService;
	}
	
	private static Logger logger = LoggerFactory.getLogger(VideoObjectConvertUtil.class);
	
	//TODO 프로퍼티로 변경
	private static String iisMeia = "http://jhlee.homedns.tv/media/";

	public static TV fileToTv(File f) {
		TV tv = new TV();
		String fileName = f.getName();
		tv.setTitle(fileName);
		tv.setFileName(fileName);
		tv.setStreamingUrl(iisMeia+fileName);
		tv.setImage("");
		tv.setLink("");
		tv.setUserRating("");
		logger.debug(String.format("%s", "File to TV=>"+tv.toString()));
		return tv;
	}
	
	public static Movie fileToMovie(File f) {
		String fileName = f.getName();
		String title = Optional.ofNullable(parseVideoName.title(fileName)).orElse(fileName);
		
		Movie newMovie = new Movie();
		newMovie.setTitle(title);
		newMovie.setFileName(fileName);
		newMovie.setStreamingUrl(iisMeia + fileName);
		newMovie.setImage("");
		newMovie.setLink("");
		newMovie.setUserRating("");
		logger.debug(String.format("%s", "File to Movie=>"+newMovie.toString()));
		return newMovie;
	}
	
	public static Movie fileToMovieWithNaverInfo(File f) {
		String fileName = f.getName();
		String movieJson = "";
		String title = parseVideoName.title(fileName);
		Movie movie = null;
		try {
			movieJson = movieService.searchMovie(title);
			movie = Optional.ofNullable(jsonToMovie(movieJson, fileName)).orElseGet(() -> {
				Movie newMovie = new Movie();
				newMovie.setTitle(title);
				newMovie.setFileName(fileName);
				newMovie.setStreamingUrl(iisMeia + fileName);

				newMovie.setImage("");
				newMovie.setLink("");
				newMovie.setUserRating("");
				return newMovie;
			});
			logger.debug(String.format("%s", "File to Movie=>"+movie.toString()));
		} catch (Exception e) {
 			logger.debug(String.format("%s, title=[%s], fileName=[%s]", e.toString(), title, fileName));
		} 
		return movie;
	}
	
	/**
	 * 새로운 타이틀로 검색하여 받아온 정보를 바탕으로 변경 전 비디오 객체와 아이디가 같은 새로운 비디오 객체 반환
	 * @param paramMap
	 * @param movieService
	 * @return
	 * @throws IOException 
	 */
	public static Movie NaverMovieInfoToMovie(Map<Object, Object> paramMap) {
		Integer vid = (Integer) paramMap.get("ID");
		String newTitle = (String) paramMap.get("NEWTITLE");
		String fileName = (String) paramMap.get("FILENAME");
		String movieInfo = "";
		Movie movie = null;
		try {
			movieInfo = movieService.searchMovie(newTitle);
			movie = jsonToMovie(movieInfo, fileName);
			movie.setFileName(fileName);
			movie.setStreamingUrl(iisMeia + fileName);
			movie.setId(vid);
		} catch (Exception e) {
			logger.debug(String.format("%s. %s", e.toString(), "Input New Title=>" + newTitle));
		}
		return movie;
	}
	
	public static Movie jsonToMovie(String item, String fileName) {
		Movie movie = null;
		Map<String, String> result = jsonStringToMap(item);
		if (!result.isEmpty()) {
			movie = new Movie();
			String title = result.get("title").replace("<b>", "").replace("</b>", "");
			String link = result.get("link");
			String image = result.get("image");
			String userRating = result.get("userRating");

			movie.setTitle(title);
			movie.setLink(link);
			movie.setImage(image);
			movie.setUserRating(userRating);
			movie.setFileName(fileName);
			movie.setStreamingUrl(iisMeia + fileName);
		} 
		return movie;
	}
	
	public static String NaverAPIUseMovieTitle(String title) {
		try {
			String movieJson = movieService.searchMovie(title);
			return movieJson;
		} catch (IOException e) {
			return "";
		}
	}
	
	public static Map<String, String> jsonStringToMap(String item) {
		LinkedHashMap<String, String> result = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = new HashMap<>();
			List<LinkedHashMap<String, String>> items = new ArrayList<>();
			map = mapper.readValue(item, new TypeReference<Map<String, Object>>() {});
			items = (List<LinkedHashMap<String, String>>) map.get("items");
			result = items.get(0);
		} catch (Exception e) {
			result = new LinkedHashMap<>();
		}
		return result;
	}
	
	/**
	 * Video(Tv, Movie)를  Map<String, Object>로 변경
	 * @param obj
	 * @return
	 */	
	public static <T extends Video> Map<String, Object> convertVideoToMap(T obj) {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			Field[] superfields = obj.getClass().getSuperclass().getDeclaredFields();
			
			for(int i=0; i<superfields.length; i++) {
				superfields[i].setAccessible(true);
				resultMap.put(superfields[i].getName().toUpperCase(), superfields[i].get(obj));
			}
			
			Field[] fields = obj.getClass().getDeclaredFields();
			for(int i=0; i<fields.length; i++) {
				fields[i].setAccessible(true);
				resultMap.put(fields[i].getName().toUpperCase(), fields[i].get(obj));
			}
		} catch(Exception e) {
			logger.debug(String.format("%s", e.getMessage()));
		}
		return resultMap;
	}
	
	/**
	 * Map에 저장된 값을 Obj의 Set Method를 이용하여 객체 생성
	 * @param m
	 * @param obj
	 * @return
	 */
	public static <T extends Video> Video convertMapToVideo(Map<String, Object> m, T obj) {
		String keyAttr = "";
		String setMethodStr = "SET";
		String methodStr = "";
		Iterator itor = m.keySet().iterator();
		while(itor.hasNext()) {
			keyAttr = (String) itor.next();
			methodStr = setMethodStr+keyAttr.substring(0, 1).toUpperCase()+keyAttr.substring(1);
			try {
				Method[] methods = obj.getClass().getSuperclass().getDeclaredMethods();
				for(int i=0; i<methods.length; i++) {
					if(methodStr.equals(methods[i].getName().toUpperCase())) {
						methods[i].invoke(obj, m.get(keyAttr));
					}
				}
			}catch(Exception e) {
				logger.debug(String.format("%s", e.getMessage()));
			}
		}
		return obj;
	}
}
