package streaming.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;

public class VideoDistinctioUtil {
	private VideoDistinctioUtil() {
		
	}
	
	public final static String[] TV_Broadcast = new String[]{"KBS", "SBS", "MBC", "Comedy TV", "tvn", "JTBC", "MBC Every1"}; 
	public final static String[] TV_Famous = new String[]{"선데이", "런닝맨", "무한도전", "정글", "혼자", "1박", "냉장고"};
	public final static String[] TV_Release = new String[]{"NEXT", "Unknown", "Rumors", "DOV", "WITH", "Euna", "LOVE", "CineBus"};
	
	public static HashMap<String, List<File>> distinctTvAndMovie(List<File> files) {
		List<File> tvPrograms = files.stream()
				.filter(f->isTvProgram(TV_Release, f.getName()))
				.filter(f->!f.getName().contains("smi"))
				.filter(f->!f.getName().contains("srt"))
				.collect(Collectors.toList());
		
		List<File> movies = new ArrayList<>(files);
		movies.removeAll(tvPrograms);
		movies = movies.stream()
				.filter(f->{
					String extenstion = FilenameUtils.getExtension(f.getName());
					if(extenstion.equals("mp4") || extenstion.equals("mkv") || extenstion.equals("avi")) {
						return true;
					} else {
						return false;
					}
				})
				.collect(Collectors.toList());
		HashMap<String, List<File>> distinctMap = new HashMap<>();
		distinctMap.put("Tv", tvPrograms);
		distinctMap.put("Movie", movies);
		return distinctMap;
	}
	
	public static boolean isTvProgram(String[] Famous, String tvFileName) {
		List<String> famousTvShow = Arrays.asList(Famous);
		for(String s: famousTvShow) {
			if(tvFileName.contains(s)) {
				return true;
			}
		}
		return false;
	}
}

