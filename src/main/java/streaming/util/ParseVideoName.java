package streaming.util;

import java.util.Properties;

import org.python.core.PyDictionary;
import org.python.core.PyUnicode;
import org.python.util.PythonInterpreter;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParseVideoName {
	
	@Autowired	
	private Logger logger;

	private static PythonInterpreter interp = null;
	private String initPTNModulePath = "";
	private boolean isInitComplete = false;
	
	public ParseVideoName() {
		Properties props = new Properties();
		props.put("python.console.encoding", "UTF-8"); // Used to prevent: console: Failed to install '':
														// java.nio.charset.UnsupportedCharsetException: cp0.
		props.put("python.security.respectJavaAccessibility", "false"); // don't respect java accessibility, so that we can access protected members on
																		// subclasses
		props.put("python.import.site", "false");
		Properties preprops = System.getProperties();

		PythonInterpreter.initialize(preprops, props, new String[0]);
		interp = new PythonInterpreter();

		// PTN 모듈 경로 추가
		try {
			initPTNModulePath = initPTNModulePath();
			interp.exec("import sys");
			interp.exec(initPTNModulePath);
			interp.exec("import PTN");
			isInitComplete = true;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private String initPTNModulePath() {
		String classesPath = this.getClass().getResource("/").getPath().substring(1).replaceAll("\\\\", "/");
		
		// PTN 모듈 경로 설정
		StringBuffer sbuf = new StringBuffer();
		sbuf.append("sys.path.append(");
		sbuf.append("'");
		sbuf.append(classesPath);
		sbuf.append("/python");
		sbuf.append("')");
		return sbuf.toString();
	}

	/**
	 * 데드락 발생 하여  synchronized 처리
	 */
	public synchronized String title(String filename) {
		try {
			logger.debug("Parse Target FileName =" + filename);
			//Junit Test 시 Bean 생성 불가능한 경우 대비
			if(!isInitComplete) {
				interp.exec("import sys");
				interp.exec(initPTNModulePath);
				interp.exec("import PTN");		
				logger.debug("Jython Init Complete");
			}
			
			PyUnicode unicodeFileName = new PyUnicode(filename);

			interp.set("str", unicodeFileName);
			interp.exec("info=PTN.parse(str)");
			PyDictionary info = (PyDictionary) interp.get("info");
			
			String parseFileName = info.get("title").toString();
			logger.debug(String.format("%s", "Parse FileName="+parseFileName));
			return parseFileName;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
