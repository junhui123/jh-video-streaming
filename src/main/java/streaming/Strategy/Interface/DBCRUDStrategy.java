package streaming.Strategy.Interface;

import java.util.List;
import java.util.Map;

public interface DBCRUDStrategy {
	void insertVideoTable(List<Map<String, Object>> videoMapList);
}
