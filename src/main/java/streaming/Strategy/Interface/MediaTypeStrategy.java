package streaming.Strategy.Interface;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface MediaTypeStrategy {
	List<Map<String, Object>> getVideoByType(List<Map<String, Object>> mapList);
	List<Map<String, Object>> convertFileToVideoAndGroupingMapList(Map<String, List<File>> map);
}
