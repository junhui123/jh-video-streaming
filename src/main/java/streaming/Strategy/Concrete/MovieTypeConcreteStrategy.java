package streaming.Strategy.Concrete;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import org.springframework.context.ApplicationContext;

import streaming.Strategy.Interface.MediaTypeStrategy;
import streaming.util.ClassDispatcher;
import streaming.util.VideoObjectConvertUtil;

public class MovieTypeConcreteStrategy implements MediaTypeStrategy {
	
	private ApplicationContext applicationContext;
	private ExecutorService exec;
	
	public MovieTypeConcreteStrategy() {
		applicationContext = ClassDispatcher.getApplicationContext();
		exec = (ExecutorService) applicationContext.getBean("Exec");
	}
	
	@Override
	public List<Map<String, Object>> getVideoByType(List<Map<String, Object>> mapList) {
		return mapList.stream().filter((Map<String, Object> s)->{
			return s.get("VIDEOTYPE").equals("Movie");
		})
		.collect(toList());
	}

	@Override
	public List<Map<String, Object>> convertFileToVideoAndGroupingMapList(Map<String, List<File>> map) {
		List<File> movieFiles = map.get("Movie");
		List<CompletableFuture<Map<String, Object>>> movies = movieFiles.stream()
					 .map(f->
						CompletableFuture.supplyAsync(()->{
							return f;
						}, exec)
					)
				   .map(future->future.thenApply(f->VideoObjectConvertUtil.fileToMovie(f)))
				   .map(future->future.thenCompose(v->
				   		CompletableFuture.supplyAsync(
							()->VideoObjectConvertUtil.convertVideoToMap(v), exec)
				   		)
					)
				   .collect(toList());
		 return movies.stream().map(CompletableFuture::join).collect(toList());	}
}
