package streaming.Strategy.Concrete;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import streaming.Strategy.Interface.DBCRUDStrategy;
import streaming.business.domain.Movie;
import streaming.business.service.VideoDao;
import streaming.util.ClassDispatcher;
import streaming.util.VideoObjectConvertUtil;

public class TVDBConcreteStrategy implements DBCRUDStrategy {

	private ApplicationContext applicationContext;
	private VideoDao videodao;
	
	public TVDBConcreteStrategy() {
		applicationContext = ClassDispatcher.getApplicationContext();
		videodao = applicationContext.getBean(VideoDao.class);
	}

	@Override
	public void insertVideoTable(List<Map<String, Object>> videoMapList) {
		for (Map<String, Object> map : videoMapList) {
			Movie mv = new Movie();
			mv = (Movie) VideoObjectConvertUtil.convertMapToVideo(map, mv);

			if (mv != null) {
				videodao.insertVideo(mv);
			}
		}
	}

}
