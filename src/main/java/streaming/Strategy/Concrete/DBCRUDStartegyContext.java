package streaming.Strategy.Concrete;

import java.util.List;
import java.util.Map;

import streaming.Strategy.Interface.DBCRUDStrategy;

public class DBCRUDStartegyContext {
	private DBCRUDStrategy crudStrategy;
	
	public DBCRUDStartegyContext(DBCRUDStrategy crudStrategy) {
		this.crudStrategy = crudStrategy;
	}

	public void insertVideoTable(List<Map<String, Object>> videoMapList) {
		crudStrategy.insertVideoTable(videoMapList);
	}
	
}
