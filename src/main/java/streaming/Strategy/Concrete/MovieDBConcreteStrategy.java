package streaming.Strategy.Concrete;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import streaming.Strategy.Interface.DBCRUDStrategy;
import streaming.business.domain.TV;
import streaming.business.service.VideoDao;
import streaming.util.ClassDispatcher;
import streaming.util.VideoObjectConvertUtil;

public class MovieDBConcreteStrategy implements DBCRUDStrategy {

	private ApplicationContext applicationContext;
	private VideoDao videodao;
	
	public MovieDBConcreteStrategy() {
		applicationContext = ClassDispatcher.getApplicationContext();
		videodao = applicationContext.getBean(VideoDao.class);
	}

	@Override
	public void insertVideoTable(List<Map<String, Object>> videoMapList) {
		for (Map<String, Object> map : videoMapList) {
			TV tv = new TV();
			tv = (TV) VideoObjectConvertUtil.convertMapToVideo(map, tv);

			if (tv != null) {
				videodao.insertVideo(tv);
			}
		}
	}

}
