package streaming.Strategy.Concrete;

import java.io.File;
import java.util.List;
import java.util.Map;

import streaming.Strategy.Interface.MediaTypeStrategy;

public class MediaTypeStrategyContext {
	private MediaTypeStrategy strategy;
	
	public MediaTypeStrategyContext(MediaTypeStrategy strategy) {
		this.strategy = strategy;
	}

	public List<Map<String, Object>> getVideoType(List<Map<String, Object>> mapList) {
		return strategy.getVideoByType(mapList);
	}
	
	public List<Map<String, Object>> convertFileToVideoAndGroupingMapList(Map<String, List<File>> map) {
		return strategy.convertFileToVideoAndGroupingMapList(map);
	}
}
