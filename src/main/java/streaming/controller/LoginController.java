package streaming.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import streaming.business.domain.VideoUser;
import streaming.business.service.VideoUserDao;

@Controller
public class LoginController {
	
	@Autowired
	VideoUserDao userDao;
	
	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	
	@CrossOrigin
	@RequestMapping(value="/login", method=RequestMethod.POST) 
	public ResponseEntity<String> login(@RequestBody Map<Object, Object> map, HttpSession session) throws Exception  {
		String uid = (String) map.get("uid");
		String rawPassword = (String) map.get("password");
		
		VideoUser user = userDao.findUserById(uid);
		if (user == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Login Fail. Check Input ID and Password");
		}
		
		/**
		 * @todo jwt 스터디 후 변경
		 */
		session.setAttribute("SESSIONID", uid);
		String encodedPassword = user.getPassword();
		boolean isPasswordMatch = passwordEncoder.matches(rawPassword, encodedPassword);
		if (isPasswordMatch) {
			return ResponseEntity.status(HttpStatus.OK).body(uid);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Login Fail. Check Input ID and Password");
		}
	}
	
	@CrossOrigin
	@RequestMapping(value="/logout", method=RequestMethod.POST) 
	public String logout(HttpSession session) {
		session.removeAttribute("SESSIONID");
		return "/";
	}
	
	@CrossOrigin
	@RequestMapping(value="/profile", method=RequestMethod.PUT) 
	public  ResponseEntity<?>  updateProfile(@RequestBody Map<Object, Object> profileMap) {
		String uid = (String) profileMap.get("uid");
		String password = (String) profileMap.get("password");
		password = passwordEncoder.encode(password);
		
		VideoUser user = new VideoUser();
		user.setUid(uid);
		user.setPassword(password);
		
		int update = userDao.updateUser(user);
		if(update > 0) {
			return ResponseEntity.status(HttpStatus.OK).body(true);
		} else {
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("Profile Update Fail. Service Unavailable");
		}
	}
	
	@CrossOrigin
	@RequestMapping(value="/profile", method=RequestMethod.GET) 
	public @ResponseBody HashMap<String, Object> profile() {
		HashMap<String, Object> profileMap = new HashMap<>();		
		return profileMap;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<String> register(@RequestBody Map<Object, Object> profileMap) throws Exception {
		String uid = (String) profileMap.get("uid");
		VideoUser user = userDao.findUserById(uid);
		int result = 0;
		if (user == null) {
			String password = (String) profileMap.get("password");
			password = passwordEncoder.encode(password);
			VideoUser NewUser = new VideoUser();
			NewUser.setUid(uid);
			NewUser.setPassword(password);
			result = userDao.insertUser(NewUser);
		}
		
		if (result > 0) {
			return ResponseEntity.status(HttpStatus.OK).body(uid);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(uid + " is already in use");
		}
	}
}
