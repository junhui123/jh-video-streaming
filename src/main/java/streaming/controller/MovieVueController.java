package streaming.controller;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import streaming.Strategy.Concrete.DBCRUDStartegyContext;
import streaming.Strategy.Concrete.MediaTypeStrategyContext;
import streaming.Strategy.Concrete.MovieDBConcreteStrategy;
import streaming.Strategy.Concrete.MovieTypeConcreteStrategy;
import streaming.Strategy.Concrete.TVDBConcreteStrategy;
import streaming.Strategy.Concrete.TVTypeConcreteStrategy;
import streaming.Strategy.Interface.DBCRUDStrategy;
import streaming.Strategy.Interface.MediaTypeStrategy;
import streaming.business.domain.Movie;
import streaming.business.domain.Video;
import streaming.business.service.NaverMovieService;
import streaming.business.service.VideoDao;
import streaming.util.VideoDistinctioUtil;
import streaming.util.VideoObjectConvertUtil;

@Controller
public class MovieVueController {

	private static boolean isIndexingNow = false;
	
	private NaverMovieService movieService;
	private VideoDao videodao;
	private ExecutorService exec;
	private Logger logger;
	private PlatformTransactionManager txManager;
	
	@Autowired
	public MovieVueController(NaverMovieService movieService, VideoDao videodao, ExecutorService exec, Logger logger, PlatformTransactionManager txManager) {
		this.movieService = movieService;
		this.videodao = videodao;
		this.exec = exec;
		this.logger = logger;
		this.txManager=txManager;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public @ResponseBody List<? extends Video> search(@RequestBody Map<Object, Object> map) {
		String query = (String) map.get("query");
		List<? extends Video> searchResult = videodao.searchVideo(query);
		return searchResult;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/favorite", method = RequestMethod.POST)
	public @ResponseBody boolean addVideoToFavorite(@RequestBody Map<Object, Object> map) {
		boolean createFavorite = false;
		createFavorite = videodao.insertFavoriteVideo((HashMap)map);
		return createFavorite;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/favorite", method = RequestMethod.DELETE)
	public @ResponseBody boolean deleteVideoToFavorite(@RequestBody Map<Object, Object> map) {
		boolean removeFavorite = false;
		removeFavorite = videodao.deleteFavoriteVideo((HashMap)map);
		return removeFavorite;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/updateMovie", method = RequestMethod.PUT)
	public ResponseEntity<?> updateMovie(@RequestBody Map<Object, Object> map) {
		ResponseEntity<?> responseEntity = null;
		Movie movie = null;
		movie = VideoObjectConvertUtil.NaverMovieInfoToMovie(map);
		if (movie != null) {
			videodao.updateVideo(movie);
			responseEntity = new ResponseEntity<Movie>(movie, HttpStatus.OK);
		} else {
			logger.debug(String.format("%s. %s", "Movie Api Call Fail", "Check Input Title"));
			responseEntity = new ResponseEntity<String>(String.format("%s. %s", "Movie Api Call Fail", "Check Input Title"), HttpStatus.SERVICE_UNAVAILABLE);
		}
		return responseEntity;
	}

	@CrossOrigin
	@RequestMapping(value = "/favorite/{uid}", method = RequestMethod.GET)
	public ResponseEntity<?> getVideoList(@PathVariable String uid, HttpServletResponse response) throws IOException {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = txManager.getTransaction(def);
		
		ResponseEntity<?> responseEntity;
		try(Stream<Path> paths = Files.walk(Paths.get("D:\\다운로드"))) {
//		try(Stream<Path> paths = Files.walk(Paths.get("\\\\JHLEE_NAS\\usbshare1\\영화"))) {
			List<File> files = paths.filter(Files::isRegularFile).map(Path::toFile).collect(toList());
			List<String> fileNames = files.stream().map(f->f.getName()).collect(toList());
			
			//즐겨찾기 체크된 비디오 리스트와 비디오 전체 리스트 비교 후 가져오기
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("fileNameList", fileNames);
			
			List<Map<String, Object>> findVideoList = getFavoriteVideoListByLoginUser(uid, paramMap);
			List<Map<String, Object>> findTvs = getVideoByType(findVideoList, new TVTypeConcreteStrategy());
			List<Map<String, Object>> findMovies = getVideoByType(findVideoList, new MovieTypeConcreteStrategy());
			
			//가져온 리스트에서 DB에 저장안된 File Insert 처리
			Map<String, List<File>> notInsertedFileListMap = getNotInsertedFile(findVideoList, files);
			CompletableFuture<List<Map<String, Object>>> f1 =CompletableFuture.supplyAsync(
									()->convertFileToVideoAndGroupingMapList(notInsertedFileListMap, new MovieTypeConcreteStrategy()), exec);
			CompletableFuture<List<Map<String, Object>>> f2 =CompletableFuture.supplyAsync(
									()->convertFileToVideoAndGroupingMapList(notInsertedFileListMap, new TVTypeConcreteStrategy()), exec);
			
			List<Map<String, Object>> movieNotInsertedList = f1.get();
			List<Map<String, Object>> tvNotInsertedList = f2.get();
			
			insertVideoTable(movieNotInsertedList, new MovieDBConcreteStrategy());
			insertVideoTable(tvNotInsertedList, new TVDBConcreteStrategy());
			
			/**
			 *아래의 형태로 반환
			 * [
			 *	 {
		     *	   "Movie": [ {...}, {...}, ... ],
			 *	   "Tv": [ {...}, {...}, ... ]
			 *	 }
			 * ]
			 */
			responseEntity = createResponseEntityInVideoList(tvNotInsertedList, findTvs, findMovies, movieNotInsertedList);
			
			//TODO 전역 변수 사용하지 않는 방법으로 변경 필요!
			if(!isIndexingNow) {
				exec.submit(()->MovieIndexing());
				isIndexingNow = true;
			}
			
			txManager.commit(status);
		} catch (Exception e) {
			logger.error(String.format("%s", e.toString()));
			responseEntity = new ResponseEntity<>(e.toString(), HttpStatus.NOT_FOUND);
			txManager.rollback(status);
		}
		return responseEntity;
	}
	
	private ResponseEntity<?> createResponseEntityInVideoList(
			List<Map<String, Object>> tvNotInsertedList,
			List<Map<String, Object>> findTvs,
			List<Map<String, Object>> findMovies,
			List<Map<String, Object>> movieNotInsertedList) {
		Map<String, List<Map<String, Object>>> tvMap = new HashMap<>();
		findTvs.addAll(tvNotInsertedList);
		tvMap.put("Tv", findTvs);
		
		Map<String, List<Map<String, Object>>> movieMap = new HashMap<>();
		findMovies.addAll(movieNotInsertedList);
		movieMap.put("Movie", findMovies); 
		
		Map<String, List<Map<String, Object>>> videoMap = new HashMap<>();
		videoMap.putAll(tvMap);
		videoMap.putAll(movieMap);
		
		List<Map<String, List<Map<String,Object>>>> videos = new ArrayList<>();
		videos.add(videoMap);	
		return ResponseEntity.status(HttpStatus.OK).body(videos);
	}
	
	/**
	 * File 객체를 Tv/Movie로 정보를 담은 Map으로 변경 후 List에 담아 반환
	 * @param videoType
	 * @param saveDB
	 * @param map
	 * @return
	 */
	private List<Map<String, Object>> convertFileToVideoAndGroupingMapList(Map<String, List<File>> map, MediaTypeStrategy strategy) {
		MediaTypeStrategyContext mediaTypeStrategyCtx = new MediaTypeStrategyContext(strategy);
		return mediaTypeStrategyCtx.convertFileToVideoAndGroupingMapList(map);
	}
	
	//TODO 줄이자 !!
	public void MovieIndexing() {
		try {
			logger.debug("Movie Indexing Start");
			ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

			List<Movie> movieList = videodao.findAllMovie();

			AtomicInteger from = new AtomicInteger(0);
			AtomicInteger to = new AtomicInteger(10);

			Runnable run = () -> {
				List<Movie> sublist = movieList.subList(from.get(), to.get());
				from.set(to.get());
				if (to.get() + 10 > movieList.size()) {
					to.set(movieList.size());
				} else {
					to.set(to.get() + 10);
				}
				for (Movie m : sublist) {
					String fileName = Optional.ofNullable(m.getFileName()).orElse("");
					if (fileName.equals(""))
						continue;

					String title = Optional.ofNullable(m.getTitle()).orElse("");
					if (title.equals(""))
						continue;

					String json = VideoObjectConvertUtil.NaverAPIUseMovieTitle(title);
					Movie movie = VideoObjectConvertUtil.jsonToMovie(json, fileName);
					if(movie==null) 
						continue;
					
					movie.setId(m.getId());
					videodao.updateVideo(movie);

					if (to.get()==movieList.size()) {
						service.shutdown();
						logger.debug("Movie Indexing End");
					}
				}
			};
			service.scheduleAtFixedRate(run, 0, 2, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * TV, Movie 타입에 따라서 VIDEO 테이블 videoType 컬럼에 입력되는 값을 다르게 하기 위한 전략 패턴 사용
	 * @param videoMapList
	 * @param crudStrategy
	 */
	private void insertVideoTable(List<Map<String, Object>> videoMapList, DBCRUDStrategy crudStrategy) {
		DBCRUDStartegyContext dbCrudCtx = new DBCRUDStartegyContext(crudStrategy);
		dbCrudCtx.insertVideoTable(videoMapList);
	}
	
	/**
	 * Video 테이블 리스트와 비교하여
	 * 저장되지 않은 파일을 Tv, Video 타입에 따라 분리
	 * @param findVideoList
	 * @param files
	 * @return
	 */
	private Map<String, List<File>> getNotInsertedFile(List<Map<String, Object>> findVideoList, List<File> files) {
		List<String> fileNameStringList = findVideoList.stream().map((Map<String, Object> s)->{
			return s.get("FILENAME").toString();
		}).collect(toList());
		
		Predicate<File> notSavedFile = p -> !fileNameStringList.contains(p.getName());
		List<File> notSavedDBList = files.stream().filter(notSavedFile).collect(toList());
		
		Map<String, List<File>> groupTvOrMovie = new HashMap<>();
		groupTvOrMovie.putAll(VideoDistinctioUtil.distinctTvAndMovie(notSavedDBList));
		
		return groupTvOrMovie;
	}
	
	/**
	 * 전체 비디오 리스트에서 유저가 즐겨찾기한 비디오 
	 * FAVORITE 값 추가한 리스트 반환
	 * @param uid
	 * @param paramMap
	 * @return
	 */
	private List<Map<String, Object>> getFavoriteVideoListByLoginUser(String uid, Map<String, Object> paramMap) {
  		//즐겨찾기 등록된 비디오 리스트 가져오기
		List<? extends Video> findFavoriteByUser = (List<? extends Video>) videodao.findFavoriteVideo(uid);
		List<Integer> userFavoriteVideoIdList = findFavoriteByUser.stream().map(v->v.getId()).collect(toList());
		
		//즐겨찾기 등록된 비디오를 전체 비디오 목록에서 제거
		List<Map<String, Object>> allVideoList = removeFavoriteVideoFromAllVideoList(paramMap, userFavoriteVideoIdList);
		
		//즐겨찾기된 비디오 map에 FAVORITE 값 삽입. 화면에서 Checkbox checked 처리에서 사용하기 위함
		List<Map<String, Object>> putFavoriteValueMapList = addFavoriteToMap(findFavoriteByUser);
		
		//정렬 후 반환
		allVideoList.addAll(putFavoriteValueMapList);
		allVideoList = sortVideoList(allVideoList);
		return allVideoList;
	}
	
	private List<Map<String, Object>> removeFavoriteVideoFromAllVideoList(Map<String, Object> paramMap, List<Integer> userFavoriteVideoIdList) {
		List<Integer> userFavoriteVideoIdListClone = new ArrayList<>(userFavoriteVideoIdList);
		//TODO findByFileNameMapList 메소드명 변경..어떤 일을 하는지 잘 모르겠음
		List<Map<String, Object>> allVideoList = videodao.findByFileNameMapList(paramMap);
		List<Map<String, Object>> putFavoriteValueMapList = allVideoList.stream().filter((Map<String, Object> s) -> {
			Integer id = (Integer) s.get("ID");
			return userFavoriteVideoIdListClone.contains(id);
		})
		.collect(toList());
		allVideoList.removeAll(putFavoriteValueMapList);
		return allVideoList;
	}
	
	private List<Map<String, Object>> addFavoriteToMap(List<? extends Video> favoriteMapList) {
		List<Map<String, Object>> addFavoriteToMapList = favoriteMapList.stream()
				.map(v->VideoObjectConvertUtil.convertVideoToMap(v))
				.collect(toList());
		
		addFavoriteToMapList = addFavoriteToMapList.stream().map((Map<String, Object> s)->{
			HashMap<String, Object> map = new HashMap<>();
			map.putAll(s);
			map.put("FAVORITE", true);
			return map;
		})
		.collect(toList());
 		return addFavoriteToMapList;
	}
	
	private List<Map<String, Object>> sortVideoList(List<Map<String, Object>> videoList) {
		List<Map<String, Object>> sortedList = new ArrayList<>(videoList);
		sortedList.sort(new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				return Integer.valueOf(o1.get("ID").toString())-Integer.valueOf(o2.get("ID").toString());
			}
		});	
		return sortedList;
	}
	
	private List<Map<String, Object>> getVideoByType(List<Map<String, Object>> mapList, MediaTypeStrategy strategy) {
		List<Map<String, Object>> list = new ArrayList<>();
		MediaTypeStrategyContext typeStrategyCtx = new MediaTypeStrategyContext(strategy);
		list = typeStrategyCtx.getVideoType(mapList);
		return list;
	}
}