package streaming.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import streaming.util.MultipartFileSender;

@Controller
public class VideoStreamingController {
	private String[] CONTENT_TYPE = new String[] {"video/mp4", "video/webm", "video/x-msvideo"};
	private String DIRECTORY_PATH = "\\\\JHLEE_NAS\\usbshare1\\영화";
	
	//TODO dot 짤리는 경우 .+ 추가
	@RequestMapping(value="/media/{fileName}", method=RequestMethod.GET)
	@CrossOrigin
	public void getContentVideo(@PathVariable String fileName, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String path = DIRECTORY_PATH+"\\"+fileName+".mp4";
		
		File file = new File(path);
				
		 try {
		      // 미디어 처리
		      MultipartFileSender
		        .fromFile(file)
		        .with(request)
		        .with(response)
		        .serveResource();
		      
		    } catch (Exception e) {
		      // 사용자 취소 Exception 은 콘솔 출력 제외
		      if (!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")) e.printStackTrace();
		    }
	}
	
	private String FindFilePath(String fileName) {
		return "";
	}
}
