package streaming.business.domain;

public class VideoUser {
	private String uid;
	private String password;
	
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("uid"+uid);
		sb.append("password="+password);
		return sb.toString();
	}
}
