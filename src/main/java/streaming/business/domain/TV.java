package streaming.business.domain;

public class TV extends Video {
	private String videoType = "Tv";
	public String getVideoType() {
		return videoType;
	}
	
	@Override
	public String toString() {
		return videoType + " " + id + " " + title + " " + link + " " + image + " " + userRating + " " + streamingUrl + " " + date + " " + fileName;
	}
}
