package streaming.business.domain;

import java.util.Date;
import java.util.Optional;

public abstract class Video {
	
	public abstract String getVideoType();
	
	protected Integer id;
	protected String fileName = "";
	protected String streamingUrl = "";
	protected String link = "";
	protected String image = "";
	protected String userRating = "";
	protected String title = "";
	protected Date date;
	
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUserRating() {
		return userRating;
	}

	public void setUserRating(String userRating) {
		this.userRating = userRating;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStreamingUrl() {
		return streamingUrl;
	}

	public void setStreamingUrl(String streamingUrl) {
		this.streamingUrl = streamingUrl;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		return 37 * Optional.ofNullable(id).orElse(Integer.valueOf(Integer.MIN_VALUE)).hashCode()
				+ fileName.hashCode()
				+ streamingUrl.hashCode()
				+ link.hashCode()
				+ image.hashCode()
				+ userRating.hashCode()
				+ title.hashCode();
	}

	public boolean equals(Video obj) {
		return Optional.ofNullable(id).orElse(Integer.valueOf(Integer.MIN_VALUE)).equals(Optional.ofNullable(obj.getId()).orElse(Integer.valueOf(Integer.MIN_VALUE)))
				&& fileName.equals(obj.getFileName())
				&& streamingUrl.equals(obj.getStreamingUrl())
				&& link.equals(obj.getLink())
				&& image.equals(obj.getImage());
	}
}
