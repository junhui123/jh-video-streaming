package streaming.business.domain;

public class Movie extends Video {

	private String videoType = "Movie";
	public String getVideoType() {
		return videoType;
	}

	@Override
	public String toString() {
		return videoType + " " + id + " " + title + " " + link + " " + image + " " + userRating + " " + streamingUrl + " " + date;
	}
}
