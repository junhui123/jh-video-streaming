package streaming.business.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NaverMovieService {
	@Value("${naver.clientID}")
	private String clientID;

	@Value("${naver.clientSecret}")
	private String clientSecret;

	@Autowired
	private Logger logger;
	
	public String searchMovie(String keyword) throws IOException {
		String item = "";
		URL url;
		url = new URL("https://openapi.naver.com/v1/search/movie.json?query=" + URLEncoder.encode(keyword, "UTF-8"));
		
		HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
		urlConn.setRequestMethod("GET");
		urlConn.setRequestProperty("X-Naver-Client-Id", clientID);
		urlConn.setRequestProperty("X-Naver-Client-Secret", clientSecret);
		urlConn.connect();

		int status = urlConn.getResponseCode();
		if (status == HttpURLConnection.HTTP_OK) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
			Stream<String> stream = reader.lines();
			item = stream.collect(Collectors.joining());
		}
		logger.debug(String.format("%s", item));
		return item;
	}
}
