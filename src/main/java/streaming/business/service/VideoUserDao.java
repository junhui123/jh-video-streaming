package streaming.business.service;

import org.apache.ibatis.annotations.Param;

import streaming.business.domain.VideoUser;

public interface VideoUserDao {
	VideoUser findUser(@Param("uid") String uid, @Param("password") String password);
	VideoUser findUserById(String uid);
	int insertUser(VideoUser user);
	int updateUser(VideoUser user);
	int deleteUser(String uid);
}
