package streaming.business.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import streaming.business.domain.Movie;
import streaming.business.domain.TV;
import streaming.business.domain.Video;

public interface VideoDao {
	public List<? extends Video> findByFileNameList(Map<String, Object> fileNameListContainMap);
	public List<Map<String, Object>> findByFileNameMapList(Map<String, Object> fileNameListContainMap);
	public Video findByFileName(@Param("fileName") String fileName);
	public Movie findByMovieTitle(@Param("title") String title);
	public Movie findByMovieFileName(@Param("fileName") String fileName);
	public TV findByTvFileName(@Param("fileName") String fileName);
	public List<Movie> findAllMovie();
	public List<TV> findAllTv();
	public List<? extends Video> findAllVideo();
	public List<? extends Video> findFavoriteVideo(String uid_fk);
	public List<? extends Video> searchVideo(String fileName);
	public boolean insertFavoriteVideo(HashMap<Object, Object> paramMap);
	public boolean deleteFavoriteVideo(HashMap<Object, Object> paramMap);
	public boolean deleteFavoriteVideoByVideoID(String videoid_fk);
	public boolean insertVideo(Video video);
	public int updateVideo(Video video);
	public boolean deleteVideo(String id);
	public boolean deleteVideoByFileName(String fileName);
}
