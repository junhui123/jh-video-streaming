package streaming.business.service;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import streaming.business.domain.Movie;
import streaming.business.domain.TV;
import streaming.util.VideoDistinctioUtil;
import streaming.util.VideoObjectConvertUtil;;

@Component
public class DirectoryWatchService {

	@Autowired
	VideoDao dao;

	@Autowired
	NaverMovieService movieService;

	@Autowired
	ExecutorService exec;
	
	private static Logger logger = LoggerFactory.getLogger(DirectoryWatchService.class);
	private WatchService watcher;
	private final Map<WatchKey, Path> keys = new HashMap<WatchKey, Path>();
	private boolean recursive = true;
	private boolean trace = false;
	private boolean isServiceStart = false;

	public boolean isRecursive() {
		return recursive;
	}

	public void setRecursive(boolean recursive) {
		this.recursive = recursive;
	}

	public boolean isTrace() {
		return trace;
	}

	public void setTrace(boolean trace) {
		this.trace = trace;
	}
	
	public boolean isServiceStart() {
		return isServiceStart;
	}
	
	public void setIsServiceStart(boolean isServiceStart) {
		this.isServiceStart = isServiceStart;
	}
	
    /**
     * 서버 시작 시 다른 스프릥 플러그인 초기화에 영향을 주어 서버 시작이 불가능 하므로
     * 별도의 스레드를 통해 Watch Service 동작
     * @param path
     */
    public void ExecutorServiceWatchDirectory(String path) {
    	if(path.isEmpty()) {
    		path = "D:\\다운로드";
    	}
    	final String newPath = path;
    	
    	Runnable run = ()->{
        	Path dir = Paths.get(newPath);
        	this.setIsServiceStart(true);
        	this.setRecursive(true);
        	this.setTrace(true);
    		try {
    			this.registerAll(dir);
    			this.processEvents();  	 		
    		} catch (IOException e) {
    		}
    	};
    	CompletableFuture.runAsync(run, exec);
    	logger.debug("ExecutorServiceWatchDirectory Start");
    }

	@PostConstruct
	public void init() {
		try {
			this.watcher = FileSystems.getDefault().newWatchService();
			ExecutorServiceWatchDirectory("");
			logger.debug("DirectoryWatchService Start");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@PreDestroy
	public void cleanup() {
		try {
			watcher.close();
	    	logger.debug("DirectoryWatchService End");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void register(Path dir) throws IOException {
		WatchKey key = dir.register(watcher, ENTRY_DELETE, ENTRY_CREATE, ENTRY_MODIFY);
		if (trace) {
			Path prev = keys.get(key);
			if (prev == null) {
				logger.debug(String.format("%s: %s", "register", dir));
			} else {
				if (!dir.equals(prev)) {
					logger.debug(String.format("%s: %s -> %s", "update", prev, dir));
				}
			}
		}
		keys.put(key, dir);
	}

	public void registerAll(final Path start) throws IOException {
		// register directory and sub-directories
		Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				register(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	public void processEvents() {
		while (true) {
			WatchKey key;
			try {
				key = watcher.take();
			} catch (InterruptedException e) {
				return;
			}

			Path dir = keys.get(key);
			if (dir == null) {
				continue;
			}

			for (WatchEvent<?> event : key.pollEvents()) {
				WatchEvent.Kind kind = event.kind();

				Path name = ((WatchEvent<Path>) event).context();
				Path child = dir.resolve(name);
				File f = child.toFile();
				String fileName = f.getName();
				boolean isTV = VideoDistinctioUtil.isTvProgram(VideoDistinctioUtil.TV_Release, fileName);
				
				//폴더 삭제 시 = ENTRY_MODIFY 실행, 파일 exists() 메소드를 통해 파일이 없다면 delete 호출
				//파일 삭제 시 = ENTRY_DELETE 실행
				//파일내용 변경 시 = ENTRY_MODIFY 이벤트 실행 ==> 상관없음
				//파일명 변경 시 = ENTRY_DELETE 후 ENTRY_CREATE 이벤트가 실행 ==> delete 쿼리 후 insert 실행
				//파일 생성 시 = ENTRY_CREATE 후 ENTRY_MODIFY 이벤트 실행
				
				logger.debug(String.format("%s: %s", event.kind().name(), child));
				if (kind == ENTRY_CREATE) {
					try {
						if (Files.isDirectory(child)) {
							registerAll(child);
						} else {
							//insert 쿼리 실행
							watchInsert(f, isTV);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					//delete 쿼리 실행
					String extension = FilenameUtils.getExtension(child.toString());
					watchDelete(kind, extension, fileName, f, isTV);
				}
			}

			boolean valid = key.reset();
			if (!valid) {
				keys.remove(key);
				if (keys.isEmpty()) {
					break;
				}
			}
		}
	}
	
	private void watchDelete(WatchEvent.Kind kind, String extension, String fileName, File f, boolean isTV) {
		if(kind == ENTRY_MODIFY) {
			if (!extension.isEmpty()) {
				if(!f.exists()) {
					watchDelete(isTV, fileName);
				} 
			}
		}
		else if(kind == ENTRY_DELETE) {
			if (!extension.isEmpty()) {
				watchDelete(isTV, fileName);
			}
		}
	}
	
	private void watchDelete(boolean isTV, String fileName) {
		if(isTV) { 
			TV tv = (TV) dao.findByFileName(fileName);
			String vid = tv.getId().toString();
			dao.deleteFavoriteVideoByVideoID(vid);
			dao.deleteVideo(vid);
		}
		else {
			Movie movie = (Movie) dao.findByFileName(fileName);
			String vid = movie.getId().toString();
			dao.deleteFavoriteVideoByVideoID(vid);
			dao.deleteVideo(vid);
		}
	}
	
	private void watchInsert(File f, boolean isTV) {
		if(isTV) {
			TV tv = VideoObjectConvertUtil.fileToTv(f);
			dao.insertVideo(tv);
		} else {
			Movie movie = VideoObjectConvertUtil.fileToMovieWithNaverInfo(f);
			dao.insertVideo(movie);
		}	
	}
}
