insert into VIDEO(videoType, title, link, image, userRating, fileName, streamingUrl) values ('Tv', '[JTBC] 냉장고를 부탁해.E162.180101.720p-NEXT.mp4', '', '', '', '[JTBC] 냉장고를 부탁해.E162.180101.720p-NEXT.mp4', 'http:....[JTBC] 냉장고를 부탁해.E162.180101.720p-NEXT.mp4')
insert into VIDEO(videoType, title, link, image, userRating, fileName, streamingUrl) values ('Tv', '[tvN] 신서유기 외전-강식당.E05.180102.720p-NEXT.mp4', '', '', '', '[tvN] 신서유기 외전-강식당.E05.180102.720p-NEXT.mp4', 'http:....[tvN] 신서유기 외전-강식당.E05.180102.720p-NEXT.mp4')
insert into VIDEO(videoType, title, link, image, userRating, fileName, streamingUrl) values ('Movie', '스파이타임', '', '', '', 'Anacleto.Agente.secreto_SPY.TIME.2015.KOR.HDRip.720p.H264.AAC-STY.mp4', 'http:....Anacleto.Agente.secreto_SPY.TIME.2015.KOR.HDRip.720p.H264.AAC-STY.mp4')
insert into VIDEO(videoType, title, link, image, userRating, fileName, streamingUrl) values ('Movie', '디버그', '', '', '', 'Debug.2014.KOR.HDRip.720p.H264.AAC-STY.mp4', 'http:....Debug.2014.KOR.HDRip.720p.H264.AAC-STY.mp4')

insert into USERS values ('user1', 'user1pwd')
insert into USERS values ('user2', 'user2pwd')
insert into USERS values ('user3', '$2a$10$W97Mnj64.DNfM31Kxi6V.epXhtTcNS9E2OMJ9TIDJPaR/ff7qnlzC')

insert into videofavorite(videoid_fk, uid_fk) values (1, 'user1')
insert into videofavorite(videoid_fk, uid_fk) values (3, 'user2')
insert into videofavorite(videoid_fk, uid_fk) values (3, 'user1')

--즐겨찾기 / 좋아요 기능에 해당하는 쿼리
select * from videofavorite where uid_fk='user1' and videoid_fk='1'

-- video 테이블 where 조인 
select video.* from video, videofavorite where video.id='1' and videofavorite.videoid_fk='1'
select video.* from video, videofavorite where video.id=videofavorite.videoid_fk
-- 같은 동작 
select v.* from video v inner join videofavorite f on v.id=f.videoid_fk
select v.* from video v left join videofavorite f on v.id=f.videoid_fk where f.favoriteid is not null
-- 조인결과 해당안되는 videofavorite 컬럼은 null 출력
select * from video v left join videofavorite f on v.id=f.videoid_fk
-- 조인 결과에 uid 조건 추가
select v.* from video v left join videofavorite f on v.id=f.videoid_fk where f.uid_fk='user1'

-- 아래 쿼리는 작동하지 않음
-- 제약 조건 위반 
DELETE FROM video WHERE video.id in (SELECT f.videoid_fk FROM videofavorite f WHERE f.videoid_fk='1' and f.uid_fk='user1')
-- hsqldb는 delete 조인 지원하지 않음...
DELETE v FROM video v INNER JOIN videofavorite f ON v.id=f.videoid_fk WHERE v.id='1' AND f.uid_fk='user1'


