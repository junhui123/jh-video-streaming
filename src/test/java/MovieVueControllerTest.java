import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import streaming.business.domain.Movie;
import streaming.business.domain.Video;
import streaming.business.service.NaverMovieService;
import streaming.business.service.VideoDao;
import streaming.controller.MovieVueController;

/**
 * 거의 동시에 최초 로그인 진행 하는 경우 같은 파일들이
 * 여러번 동시에 DB에 추가 되는지 테스트 테스트 결과 JSON 반환
 * HSQLDB Manager를 통해 저장되어 있는지 확인
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/appconfig-biz.xml"})
@Transactional
public class MovieVueControllerTest {

	@Mock
	NaverMovieService movieService;
	
	@Mock
	VideoDao videodao;
	
	@Mock
	ExecutorService exec;

	@InjectMocks
	private MovieVueController ctl;
	
	@Autowired
	private WebApplicationContext wac;
	
	private MockMvc mockMvc;

	@Before
	public void setUp() {
		// Mockito 어노테이션 선언 변수들 객체 생성
		MockitoAnnotations.initMocks(this);
	    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testFavorite() throws Exception {
		MvcResult result = this.mockMvc.perform(get("http://localhost:8080/favorite/user1"))
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
					.andDo(print())
					.andReturn();
		
		ObjectMapper mapper = new ObjectMapper();
		List<Map<String, Object>> resultList = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Map<String, Object>>>() {});

		List<String> resultFileNameList = getMvcResultFileNameList(resultList);
		List<String> fileNameList = getFileNameList("D:\\다운로드");

		assertEquals(resultFileNameList.size(), fileNameList.size());
	}
	
	private List<String> getFileNameList(String path) throws IOException {
		Stream<Path> paths = Files.walk(Paths.get(path));
		List<File> files = paths.filter(Files::isRegularFile).map(Path::toFile).collect(toList());
		List<String> fileNameList = files.stream().map(f -> f.getName()).collect(toList());
		fileNameList = fileNameList.stream().filter(p -> {
			if (p.contains("mp4") || p.contains("mkv") || p.contains("avi")) {
				return true;
			} else {
				return false;
			}
		}).map(m -> m).collect(toList());
		
		Collections.sort(fileNameList);
		
		return fileNameList;
	}
	
	private List<String> getMvcResultFileNameList(List<Map<String, Object>> mapList) {
		Map<String, Object> resultMap =  mapList.get(0);
		List<Map<String, Object>> MovieMapList = (List<Map<String, Object>>) resultMap.get("Movie");
		List<Map<String, Object>> TVMapList = (List<Map<String, Object>>) resultMap.get("Tv");
		
		List<String> MoviefileNameList = MovieMapList.stream().map(m->m.get("FILENAME").toString()).collect(toList());
		List<String> TVfileNameList = TVMapList.stream().map(m->m.get("FILENAME").toString()).collect(toList());
		
		MoviefileNameList.addAll(TVfileNameList);
		Collections.sort(MoviefileNameList);
		return MoviefileNameList;
	}
	
	@Test
	@Ignore
	public void testVideoIndexing() throws Exception {
		List<Movie> movies = new ArrayList();
		List<Movie> limitMovies = movies.stream().limit(10).collect(Collectors.toList());
		List<Movie> updated10 = null; //ctl.videoIndexing(limitMovies);
		
		assertEquals(limitMovies, updated10);
	}
}
