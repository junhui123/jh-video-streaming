import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import streaming.business.domain.TV;

public class TVObjectTEST {
	
	TV tv1 ;
	TV tv2 ;
	TV tv3 ;
	
	@Before
	public void setUp() {
		tv1 = makeTV("tv1");
		tv2 = makeTV("tv2");
		tv3 = makeTV("tv3");
	}
	
	@Test
	public void testTVEqual() {
		TV tv4 = makeTV("tv1");
		//assertEquals(true, tv1.equals(tv4));
		assertTrue(tv1.equals(tv4));
		assertFalse(tv1.equals(tv2));
		assertFalse(tv2.equals(tv3));
		assertFalse(tv3.equals(tv1));
	}
	
	@Test
	public void testTVListEqual() {
		List<TV> tvlist1 = new ArrayList<>();
		tvlist1.add(tv1);
		tvlist1.add(tv2);
		tvlist1.add(tv3);
		
		List<TV> tvlist2 = new ArrayList<>();
		tvlist2.add(tv1);
		tvlist2.add(tv2);
		tvlist2.add(makeTV("tv4"));
		assertNotEquals(tvlist1, tvlist2);
		
		tv1.setFileName("tv11.mkv");
		tv1.setTitle("tv11");
		List<TV> tvlist3 = new ArrayList<>();
		tvlist3.add(tv1);
		tvlist3.add(tv2);
		tvlist3.add(tv3);
		
		assertEquals(tvlist1.get(0).hashCode(), tvlist3.get(0).hashCode());
		assertTrue(tvlist1.equals(tvlist3));
	}
	
	private TV makeTV(String name) {
		TV tv = new TV();
		tv.setFileName(name+".mp4");
		tv.setTitle(name);
		tv.setImage(name+" image");
		tv.setDate(null);
		tv.setLink(name+" link");
		tv.setStreamingUrl(name+" streming");
		tv.setUserRating("9.1");
		return tv;
	}
}
