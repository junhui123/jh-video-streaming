import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import streaming.business.domain.Movie;
import streaming.business.service.NaverMovieService;
import streaming.util.ParseVideoName;
import streaming.util.VideoObjectConvertUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/appconfig-biz.xml"})
@Transactional
public class NaveMovieAPITest {

	@Autowired
	private NaverMovieService m;

	ParseVideoName parseVideoName;
	
	@Before
	public void Setup() {
		parseVideoName = new ParseVideoName();
	}
	
	@Test
	public void getOne() throws IOException {
		String movie = m.searchMovie("A Taxi Driver");
		assertNotNull(movie);
	}
	
	@Test
	public void getMultiple() throws IOException {
		//네이버 1초당 10건으로 제한되어 있음...
		Stream<Path> paths = Files.walk(Paths.get("\\\\JHLEE_NAS\\usbshare1\\영화"));
		List<File> files = paths.filter(Files::isRegularFile).map(Path::toFile).collect(toList());
		List<String> fileNames = files.stream().map(f->f.getName()).collect(toList());
		for(String s : fileNames) {
			String title = parseVideoName.title(s);
			String movie = m.searchMovie(title);
			assertNotNull(movie);
		}
	}

	@Test
	public void jsonToMovieObject() {
		String jsonStr = "{\r\n" + "	\"lastBuildDate\": \"Mon, 15 Jan 2018 23:28:03 +0900\",\r\n" + "	\"total\": 1,\r\n" + "	\"start\": 1,\r\n" + "	\"display\": 1,\r\n"
				+ "	\"items\": [{\r\n" + "			\"title\": \"오만과 편견 그리고 좀비\",\r\n" + "			\"link\": \"http://movie.naver.com/movie/bi/mi/basic.nhn?code=87585\",\r\n"
				+ "			\"image\": \"http://imgmovie.naver.com/mdi/mit110/0875/87585_P27_105921.jpg\",\r\n" + "			\"subtitle\": \"<b>Pride and Prejudice and Zombies</b>\",\r\n"
				+ "			\"pubDate\": \"2016\",\r\n" + "			\"director\": \"버 스티어스|\",\r\n" + "			\"actor\": \"릴리 제임스|샘 라일리|잭 휴스턴|벨라 헤스콧|더글러스 부스|맷 스미스|찰스 댄스|레나 헤디|\",\r\n"
				+ "			\"userRating\": \"6.29\"\r\n" + "		}\r\n" + "	]\r\n" + "}\r\n" + "";
		try {
			Movie m = VideoObjectConvertUtil.jsonToMovie(jsonStr, "Pride.and.Prejudice.and.Zombies.2016.720p.HDRip.H264-PCHD.mkv");
			assertNotNull(m);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}