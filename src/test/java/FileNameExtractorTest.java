import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import streaming.util.ParseVideoName;

public class FileNameExtractorTest {

	ParseVideoName parseVideoName;
	
	@Before
	public void setUp() {
		parseVideoName = new ParseVideoName();
	}
	
	@Test
	public void test() {
		String title = parseVideoName.title("Blade.Runner.2049.1080p.WEB-DL.x265.HEVC.6CH-MRN");
		assertEquals("Blade Runner 2049", title);
	}
}
