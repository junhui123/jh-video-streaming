import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import streaming.business.domain.Movie;
import streaming.business.domain.TV;
import streaming.business.domain.Video;
import streaming.business.service.VideoDao;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/appconfig-biz.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DAOTest {

	@Autowired
	private VideoDao dao;
	
	@Autowired
	private PlatformTransactionManager txManager;
	
	@Test
	public void test1insert() {
		Movie m = new Movie();
		m.setTitle("아이 오리진스");
		m.setLink("");
		m.setImage("");
		m.setUserRating("");
		m.setStreamingUrl("http://MovieIISTESTURL/I.Origins.2014.1080p.BluRay.x264.DTS-ONe.mkv");
		m.setDate(new Date());
		m.setFileName("I.Origins.2014.1080p.BluRay.x264.DTS-ONe.mkv");
		boolean condition = dao.insertVideo((Video)m);
		assertTrue(condition);
		
		m = new Movie();
		m.setTitle("블레이드 러너 2049");
		m.setLink("http://movie.naver.com/movie/bi/mi/basic.nhn?code=88227");
		m.setImage("http://imgmovie.naver.com/mdi/mit110/0882/88227_P24_113936.jpg");
		m.setUserRating("7.96");
		m.setStreamingUrl("http://MovieIISTESTURL/Blade.Runner.2049.1080p.WEB-DL.x265.HEVC.6CH-MRN.mkv");
		m.setDate(new Date());
		m.setFileName("Blade.Runner.2049.1080p.WEB-DL.x265.HEVC.6CH-MRN.mkv");
		condition = dao.insertVideo((Video)m);
		assertTrue(condition);
		
		m = new Movie();
		m.setTitle("나는 고양이로소이다");
		m.setLink("");
		m.setImage("");
		m.setUserRating("");
		m.setStreamingUrl("http://MovieIISTESTURL/나는고양이로소이다.mp4");
		m.setDate(new Date());
		m.setFileName("나는고양이로소이다.mp4");
		condition = dao.insertVideo((Video)m);
		assertTrue(condition);
	}
	
	@Test
	public void test2find() {
		List<Movie> movies = dao.findAllMovie();
		assertNotNull(movies);
		
		Movie m = dao.findByMovieTitle("아이 오리진스");
		assertEquals("아이 오리진스", m.getTitle());
	}
	
	@Test
	public void test3update() {
		Movie originM = dao.findByMovieTitle("아이 오리진스");
		
		Movie m = new Movie();
		m.setId(originM.getId());
		m.setTitle("아이 오리진스");
		m.setLink("http://movie.naver.com/movie/bi/mi/basic.nhn?code=112077");
		m.setImage("\"http://imgmovie.naver.com/mdi/mit110/1120/112077_P04_111316.jpg");
		m.setUserRating("8.42");
		m.setStreamingUrl("http://MovieIISTESTURL/I.Origins.2014.1080p.BluRay.x264.DTS-ONe.mkv");
		m.setDate(new Date());
		m.setFileName("I.Origins.2014.1080p.BluRay.x264.DTS-ONe.mkv");		
		int updateCount = dao.updateVideo(m);
		
		Movie newM = dao.findByMovieTitle("아이 오리진스");
		
		assertNotEquals(originM, newM);
		assertEquals(1, updateCount);
	}
	
	@Test
	public void test4InsertTransaction() {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = txManager.getTransaction(def);
		
		TV tv = new TV();
		tv.setTitle("런닝맨.E525.180325.720p-NEXT");
		tv.setLink("");
		tv.setImage("");
		tv.setUserRating("");
		tv.setStreamingUrl("http://MovieIISTESTURL/런닝맨.E525.180325.720p-NEXT.mp4");
		tv.setDate(new Date());
		tv.setFileName("런닝맨.E525.180325.720p-NEXT.mp4");
		
		TV tv2 = new TV();
		tv2.setTitle("[JTBC] 아는 형님.E120.180324.720p-NEXT");
		tv2.setLink("");
		tv2.setImage("");
		tv2.setUserRating("");
		tv2.setStreamingUrl("http://MovieIISTESTURL/[JTBC] 아는 형님.E120.180324.720p-NEXT");
		tv2.setDate(new Date());
		tv2.setFileName("[JTBC] 아는 형님.E120.180324.720p-NEXT.mp4");
		
		try {
			insertException(tv, tv2);
			txManager.commit(status);
		} catch (Exception e) {
			txManager.rollback(status);
		}
		
		List<TV> tvs = dao.findAllTv();
		assertTrue("등록된 TV 오브젝트 없음", tvs.isEmpty());
	}
	
	private void insertException(TV tv, TV tv2) throws Exception {
		dao.insertVideo(tv);
		dao.insertVideo(tv2);
		throw new Exception();
	}
	
	@Test
	public void test5UpdateTransaction() {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = txManager.getTransaction(def);
		Movie m = dao.findByMovieTitle("아이 오리진스");
		try {
			m.setTitle("잘못 설정된 타이틀");
			updateException(m);
			txManager.commit(status);
		} catch (Exception e) {
			txManager.rollback(status);
		}
		Movie find = dao.findByMovieTitle("아이 오리진스");
		assertTrue("타이틀 롤백", "아이 오리진스".equals(find.getTitle()));
		assertTrue("타이틀 롤백 아이디 비교", m.getId().equals(find.getId()));
	}
	
	private void updateException(Movie m) throws Exception {
		dao.updateVideo(m);
		throw new Exception();
	}
	
	@Test
	public void test6findAll() {
		List<? extends Video> videos = dao.findAllVideo();
		assertEquals("현재 등록된 건수 3건", 3, videos.size()); 
	}
	
	@Test
	public void test7delete() {
		assertTrue(dao.deleteVideoByFileName("I.Origins.2014.1080p.BluRay.x264.DTS-ONe.mkv"));
		assertTrue(dao.deleteVideoByFileName("Blade.Runner.2049.1080p.WEB-DL.x265.HEVC.6CH-MRN.mkv"));
		assertTrue(dao.deleteVideoByFileName("나는고양이로소이다.mp4"));
		assertFalse(dao.deleteVideoByFileName("abc"));
	}
}
