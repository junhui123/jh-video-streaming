import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import streaming.business.service.DirectoryWatchService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/appconfig-biz.xml"})
public class WatchServiceTest {

	@Autowired
	private ApplicationContext ctx;
	
	@Test
	public void test() {
		DirectoryWatchService watch = ctx.getBean(DirectoryWatchService.class);
		Path dir = Paths.get("D:\\다운로드");
		watch.setRecursive(true);
		watch.setTrace(true);
		try {
			watch.registerAll(dir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		watch.processEvents();
	}
}
