import static org.junit.Assert.assertEquals;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import streaming.business.domain.VideoUser;
import streaming.business.service.VideoUserDao;

/**
 * 유저 생성 테스트
 * 
 * 참고 https://m.blog.naver.com/seuis398/220313514110
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/appconfig-biz.xml"})
@Transactional
public class UserCreateTest {

	@Autowired
	VideoUserDao userDao;
	
	@Ignore
	@Test
	public void test1() {
		VideoUser user = new VideoUser();
		user.setUid("Test1234");
		user.setPassword("pwTest1234");
		userDao.insertUser(user);
		VideoUser findUser = userDao.findUserById("Test1234");
		assertEquals("Test1234", "Test1234");
	}
	
	@Ignore
	@Test
	public void test2() {
		VideoUser user = new VideoUser();
		user.setUid("Test1234");
		user.setPassword("pwTest1234");
		userDao.insertUser(user);
		VideoUser findUser = userDao.findUserById("Test1234");
		assertEquals("Test1234", "Test1234");
	}
	
	/**
	 * 같은 UID 비동기 생성
	 */
	@Test
	public void test3() {
		Integer user1Success = null;
		Integer user2Success = null;
		
		try {
			CompletableFuture<Integer> future = CompletableFuture.supplyAsync(()-> {
				VideoUser user1 = new VideoUser();
				user1.setUid("TOP123");
				user1.setPassword("112233");
				return userDao.insertUser(user1);
			});
			user1Success = future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
		VideoUser user2 = new VideoUser();
		user2.setUid("TOP123");
		user2.setPassword("445566");
		user2Success = userDao.insertUser(user2);
		
		assertEquals(user1Success, user2Success);
	}
	
	/**
	 * 서로 다른  UID 비동기 생성
	 */
	@Test
	public void test4() {
		Integer user1Success = null;
		Integer user2Success = null;
		
		try {
			CompletableFuture<Integer> future = CompletableFuture.supplyAsync(()-> {
				VideoUser user1 = new VideoUser();
				user1.setUid("qwer1234");
				user1.setPassword("112233");
				return userDao.insertUser(user1);
			});
			user1Success = future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
		VideoUser user2 = new VideoUser();
		user2.setUid("zxcv1234");
		user2.setPassword("445566");
		user2Success = userDao.insertUser(user2);
		
		assertEquals(user1Success, user2Success);
	}
}
